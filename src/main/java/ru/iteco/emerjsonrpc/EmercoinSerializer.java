package ru.iteco.emerjsonrpc;


import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import ru.iteco.emerjsonrpc.protocol.JsonRpcRequestBody;
import ru.iteco.emerjsonrpc.protocol.NameListResponseBody;
import ru.iteco.emerjsonrpc.protocol.NameValuePair;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.*;
import java.util.Arrays;
import java.util.List;

/**
 * Класс для загрузки и выгрузки из блокчейна
 */
public class EmercoinSerializer {

    private static final String METHOD_NAME_LIST = "name_list";
    private static final String METHOD_NAME_NEW = "name_new";
    private static Logger log = Logger.getLogger(EmercoinSerializer.class);

    private Gson gson;
    private JsonRpcClient rpcClient;

    public EmercoinSerializer() {
        gson = new Gson();
        rpcClient = new JsonRpcClient();
    }

    /**
     * Выгрузка данных из блокчейна в файлы в определенную директорию
     * @param directoryPath путь к директории, куда сохранять файлы
     */
    public void loadFromBlckchainToFiles(String directoryPath) {

        Path directory = Paths.get(directoryPath);
        if (!Files.exists(directory)) {
            try {
                Files.createDirectories(directory);
            } catch (IOException e) {
                log.error("Ошибка при попытке создать директорию: " + directoryPath, e);
            }
        }

        JsonRpcRequestBody rpcBody = new JsonRpcRequestBody();
        rpcBody.setMethod(METHOD_NAME_LIST);
        String body = gson.toJson(rpcBody);
        String responseJson = rpcClient.executePost(body);

        NameListResponseBody responseBody = null;
        try {
            responseBody = gson.fromJson(responseJson, NameListResponseBody.class);
        } catch (JsonSyntaxException ex) {
            log.error("Ошибка при запросе " + METHOD_NAME_LIST, ex);
        }

        if (responseBody != null) {
            List<NameValuePair> names = responseBody.getNames();
            if (names != null) {
                names.forEach(nameValuePair -> saveToFile(nameValuePair, directory));
            }
        }
    }

    private void saveToFile(NameValuePair nameValuePair, Path directory) {
        Path outputFile = Paths.get(directory.toString(), nameValuePair.getName());
        try {
            Files.createFile(outputFile);
        } catch (IOException e) {
            log.error("Ошибка при создании файла: " + outputFile);
        }

        try(BufferedWriter writer = Files.newBufferedWriter(outputFile,
                StandardOpenOption.TRUNCATE_EXISTING)) {


            writer.write(nameValuePair.getValue());
        } catch (IOException e) {
            log.error("Ошибка при сохранении файла " + outputFile, e);
        }
    }

    /**
     * Запись данных из файла в блокчейн
     * @param filePath путь к файлу
     * @param days срок
     */
    public void uploadFromFileToBlockChain(String filePath, int days) {
        Path path = Paths.get(filePath);

        String value = null;
        try {
            byte[] fileBytes = Files.readAllBytes(path);
            value = Base64.encodeBase64String(fileBytes);
        } catch (IOException e) {
            log.error("Ошибка при чтении файла " + filePath);
        }

        if (value != null) {
            JsonRpcRequestBody requestBody = new JsonRpcRequestBody();
            requestBody.setMethod(METHOD_NAME_NEW);
            List<String> params = Arrays.asList(path.getFileName().toString(), value, String.valueOf(days));
            requestBody.setParams(params);

            String content = gson.toJson(requestBody);
            rpcClient.executePost(content);
        }
    }
}
