package ru.iteco.emerjsonrpc;

import okhttp3.*;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Properties;

/**
 * JSON-RPC клиент для работы с Emercoin
 * параметры конфигурации берет из ресурсов: resources/emercoin.properties
 */
public class JsonRpcClient {

    private static final Logger log = Logger.getLogger(JsonRpcClient.class);

    private HttpUrl baseUrl;
    private String credentials;
    private OkHttpClient httpClient;

    public JsonRpcClient() {

        httpClient = new OkHttpClient.Builder().build();

        Properties properties = new Properties();
        try {
            properties.load(JsonRpcClient.class.getClassLoader()
                    .getResourceAsStream("emercoin.properties"));

            baseUrl = new HttpUrl.Builder().scheme("http")
                    .host(properties.getProperty("rpchost"))
                    .port(Integer.valueOf(properties.getProperty("rpcport")))
                    .build();

            credentials = Credentials.basic(properties.getProperty("rpcuser"),
                    properties.getProperty("rpcpassword"));

        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * Метод выполняет POST запрос
     * @param content тело запроса - строка с JSON
     * @return JSON строка с ответом или null в случае неудачи
     */
    public String executePost(String content) {

        RequestBody body = RequestBody.create(MediaType.parse("text/plain"), content);
        Request request = new Request.Builder()
                .addHeader("Authorization", credentials)
                .url(baseUrl).post(body).build();

        String responseBody = null;
        try {
            Response response = httpClient.newCall(request).execute();
            if (response.isSuccessful()) {
                responseBody = response.body().string();
            }
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }

        return responseBody;
    }
}
