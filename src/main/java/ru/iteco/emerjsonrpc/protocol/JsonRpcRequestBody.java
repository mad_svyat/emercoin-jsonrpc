package ru.iteco.emerjsonrpc.protocol;

import java.util.ArrayList;
import java.util.List;

public class JsonRpcRequestBody {

    private String jsonrpc;
    private String method;
    private List<String> params;

    public JsonRpcRequestBody() {
        jsonrpc = "1.0";
        params = new ArrayList<>();
    }

    public String getJsonrpc() {
        return jsonrpc;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public List<String> getParams() {
        return params;
    }

    public void setParams(List<String> params) {
        this.params = params;
    }
}
