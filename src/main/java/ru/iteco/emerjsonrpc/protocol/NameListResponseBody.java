package ru.iteco.emerjsonrpc.protocol;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class NameListResponseBody {

    @SerializedName("result")
    private List<NameValuePair> names;
    private String error;
    private String id;

    public NameListResponseBody() {
        names = new ArrayList<>();
    }

    public List<NameValuePair> getNames() {
        return names;
    }

    public void setNames(List<NameValuePair> names) {
        this.names = names;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
