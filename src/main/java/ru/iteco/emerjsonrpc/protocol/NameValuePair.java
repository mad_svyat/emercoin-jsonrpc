package ru.iteco.emerjsonrpc.protocol;

import com.google.gson.annotations.SerializedName;

public class NameValuePair {

    private String name;
    private String value;
    private String address;
    @SerializedName("expires_in")
    private long expiresIn;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(long expiresIn) {
        this.expiresIn = expiresIn;
    }
}
