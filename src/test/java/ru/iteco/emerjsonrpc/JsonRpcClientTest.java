package ru.iteco.emerjsonrpc;

import org.junit.Test;

import static org.junit.Assert.*;


public class JsonRpcClientTest {

    @Test
    public void request() throws Exception {

        JsonRpcClient rpcClient = new JsonRpcClient();
        String response = rpcClient.executePost("{\"jsonrpc\": \"1.0\", \"method\": \"name_list\", \"params\":[] }");

        assertNotNull(response);

        System.out.println(response);
    }
}